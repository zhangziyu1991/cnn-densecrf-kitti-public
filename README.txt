===== Description =====

This software pertains to the research described in the CVPR 2016 paper:
Instance-Level Segmentation for Autonomous Driving with Deep Densely Connected MRFs
Ziyu Zhang, Sanja Fidler and Raquel Urtasun

In annotations/, we make our manual car annotations publicly available.

In cnn/, we provide trained deeplab model for segmenting and depth
ordering instances in local image patches.

In densecrf/, we provide the CRF framework we use for merging patches.

In image_sets/, we list the training/validation/test split we use in
our paper. All images in three sets come from the training set of
KITTI's object detection benchmark.

===== LICENSE =====

Copyright (C) 2016  Ziyu Zhang, Sanja Fidler and Raquel Urtasun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Any commercial use is strictly prohibited except by explicit permission by the authors. 
For more information on commercial use, contact Raquel Urtasun (urtasun@cs.toronto.edu).

===== USAGE =====

1. Extract image patches and store extracted patches somewhere.
2. Generate test_formatted.txt and test_id.txt files and store in cnn/
    1) In test_formatted.txt, list patches each line formatted as
       /path/to/patch_name.png,
    2) In test_id.txt, list patches each line formatted as patch_name.
3. Run cnn/run_test.sh to obtain patch-level predictions. Make sure you   
   upsample each output volume to its input patch's original size and 
   store it as float binary in row-column-channel order.
   (See densecrf/data/unary for example.)
4. To run our patch merging MRF, prepare data as exemplified in    
   densecrf/data, including:
    1) original KITTI image,
    2) region of interest in the format of (y1, y2, x1, x2) (1-based, both       
       end inclusive),
    3) CNN predictions obtained from step 3 stored as float binary.
5. Adjust the paths, i.e., IMAGE_FOLDER, UNARY_FOLDER and ROI_FOLDER in 
   densecrf/inference/inference.cpp to be absolute path to where you store 
   the data in step 4. 
6. Make sure you check and modify densecrf/RunInference.bash, and run it.

For questions and suggestions, please contact Ziyu Zhang (zzhang@cs.toronto.edu).