/*
    Copyright (c) 2013, Philipp Krähenbühl
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
        * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
        * Neither the name of the Stanford University nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY Philipp Krähenbühl ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL Philipp Krähenbühl BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "densecrf.h"
#include "permutohedral.h"
#include "util.h"
#include "pairwise.h"
#include <cmath>
#include <cstring>
#include <iostream>
#include <fstream>

/////////////////////////////
/////  Alloc / Dealloc  /////
/////////////////////////////
DenseCRF::DenseCRF(int N, int M) : N_(N), M_(M), unary_(0) {
}
DenseCRF::~DenseCRF() {
	if (unary_)
		delete unary_;
	for( unsigned int i=0; i<pairwise_.size(); i++ )
		delete pairwise_[i];
}
DenseCRF2D::DenseCRF2D(int W, int H, int M) : DenseCRF(W*H,M), W_(W), H_(H) {
}
DenseCRF2D::~DenseCRF2D() {
    for( unsigned int i=0; i<compatibility_.size(); i++ )
        delete compatibility_[i];
}
/////////////////////////////////
/////  Pairwise Potentials  /////
/////////////////////////////////
void DenseCRF::addPairwiseEnergy (const MatrixXf & features, LabelCompatibility * function, KernelType kernel_type, NormalizationType normalization_type, bool eval, const MatrixXf & features_eval) {
	//assert( features.cols() == N_ );
	addPairwiseEnergy( new PairwisePotential( features, function, kernel_type, normalization_type, eval, features_eval ) );
}
void DenseCRF::addPairwiseEnergy ( PairwisePotential* potential ){
	pairwise_.push_back( potential );
}
/*void DenseCRF2D::addPairwiseGaussian(float sx, float sy, LabelCompatibility * function, std::vector<int> region, KernelType kernel_type, NormalizationType normalization_type) {
	MatrixXf feature( 3, N_ );
    MatrixXf feature_eval( 3, N_ );
	for( int j=0; j<H_; j++ )
		for( int i=0; i<W_; i++ ){
			feature(0,j*W_+i) = i / sx;
			feature(1,j*W_+i) = j / sy;
            feature(2,j*W_+i) = 0;
            
            feature_eval(0,j*W_+i) = 0;
            feature_eval(1,j*W_+i) = i / sx;
            feature_eval(2,j*W_+i) = j / sy;
		}
	//addPairwiseEnergy( feature, function, kernel_type, normalization_type );
    if (region.empty()) {
        region.push_back(0);
        region.push_back(H_);
        region.push_back(0);
        region.push_back(W_);
    }
    region_.push_back(region);
    region2_.push_back(std::vector<int>());
    compatibility_.push_back(NULL);
    //assert( feature.cols() == (region[2] - region[0])*(region[3] - region[1]) );
    addPairwiseEnergy( feature, function, kernel_type, normalization_type, true, feature_eval );
}*/
void DenseCRF2D::addPairwiseGaussian (float sx, float sy, LabelCompatibility * function, std::vector<int> region, KernelType kernel_type, NormalizationType normalization_type) {
    MatrixXf feature( 2, N_ );
    for( int j=0; j<H_; j++ )
        for( int i=0; i<W_; i++ ){
            feature(0,j*W_+i) = i / sx;
            feature(1,j*W_+i) = j / sy;
        }
    region_.push_back(region);
    region2_.push_back(std::vector<int>());
    compatibility_.push_back(NULL);
    addPairwiseEnergy(feature, function, kernel_type, normalization_type);
}
void DenseCRF2D::addPairwiseBilateral ( float sx, float sy, float sr, float sg, float sb, const unsigned char* im, LabelCompatibility * function, std::vector<int> region, KernelType kernel_type, NormalizationType normalization_type) {
	MatrixXf feature( 5, N_ );
	for( int j=0; j<H_; j++ )
		for( int i=0; i<W_; i++ ){
			feature(0,j*W_+i) = i / sx;
			feature(1,j*W_+i) = j / sy;
			feature(2,j*W_+i) = im[(i+j*W_)] / sr;
			feature(3,j*W_+i) = im[(i+j*W_)+N_] / sg;
			feature(4,j*W_+i) = im[(i+j*W_)+2*N_] / sb;
		}
    region_.push_back(region);
    region2_.push_back(std::vector<int>());
    compatibility_.push_back(NULL);
    addPairwiseEnergy(feature, function, kernel_type, normalization_type);
}
void DenseCRF2D::addLocalPairwiseGaussian(float sx, float sy, LabelCompatibility * function, std::vector<int> region, KernelType kernel_type, NormalizationType normalization_type) {
    int H = region[1] - region[0];
    int W = region[3] - region[2];
    int N = H * W;
    MatrixXf feature(2, N);
    for (int j = 0; j < H; ++j) {
        for (int i = 0; i < W; ++i) {
            feature(0,j*W+i) = i / sx;
            feature(1,j*W+i) = j / sy;
        }
    }
    region_.push_back(region);
    region2_.push_back(std::vector<int>());
    compatibility_.push_back(NULL);
    addPairwiseEnergy(feature, function, kernel_type, normalization_type);
}
void DenseCRF2D::addLocalPairwiseBilateral(float sx, float sy, float sp, const Matrix<float, Dynamic, Dynamic, RowMajor> & prob, LabelCompatibility * function, std::vector<int> region, KernelType kernel_type, NormalizationType normalization_type) {
    int H = region[1] - region[0];
    int W = region[3] - region[2];
    int N = H*W;
    int M = prob.rows();
    MatrixXf feature(M+2, N);
    for (int j = 0; j < H; ++j) {
        for (int i = 0; i < W; ++i) {
            feature(0,j*W+i) = i / sx;
            feature(1,j*W+i) = j / sy;
        }
    }
    feature.block(2,0,M,N) = prob / sp;
    region_.push_back(region);
    region2_.push_back(std::vector<int>());
    compatibility_.push_back(NULL);
    addPairwiseEnergy(feature, function, kernel_type, normalization_type);
}
void DenseCRF2D::addPairwiseLocal(int t, float sp, const Matrix<float, Dynamic, Dynamic, RowMajor> & prob, LabelCompatibility * function, std::vector<int> region, KernelType kernel_type, NormalizationType normalization_type) {
    int H = region[1] - region[0];
    int W = region[3] - region[2];
    int N = H*W;
    int M = prob.rows();
    MatrixXf feature = Eigen::MatrixXf::Zero(M + std::abs(t), N);
    MatrixXf feature_eval = Eigen::MatrixXf::Zero(M + std::abs(t), N);
    
    if (t > 0) {
        feature.block(0, 0, M, N) = prob / sp;
        feature_eval.block(t, 0, M, N) = prob / sp;
    } else if (t == 0) {
        feature = prob / sp;
        feature_eval = prob / sp;
    } else {
        feature.block(-t, 0, M, N) = prob / sp;
        feature_eval.block(0, 0, M, N) = prob / sp;
    }
    region_.push_back(region);
    region2_.push_back(std::vector<int>());
    compatibility_.push_back(NULL);
    addPairwiseEnergy( feature, function, kernel_type, normalization_type, true, feature_eval );
}
void DenseCRF2D::addPairwiseVertical(int t, float sp, float sp2, int numSlices, Matrix<float, Dynamic, Dynamic, RowMajor> & globalBinaryProb, LabelCompatibility * function, std::vector<int> region, KernelType kernel_type, NormalizationType normalization_type) {
    //MatrixXf feature_raw(numSlices, N_);
    MatrixXf feature_raw(1, N_);
    feature_raw.fill(1.0);
    /*for (int j = 0; j < H_; ++j) {
        for (int i = 0; i < W_; ++i) {
            //feature_raw(numSlices*(H_-1-j)/H_, j*W_+i) = 1 / sp;
            feature_raw(0, j*W_+i) = j / sp;
            feature_raw(1, j*W_+i) = globalBinaryProb(1, j*W_+i) / sp2;
        }
    }*/
    
    /*MatrixXf feature = Eigen::MatrixXf::Zero(numSlices + std::abs(t), N_);
    MatrixXf feature_eval = Eigen::MatrixXf::Zero(numSlices + std::abs(t), N_);
    
    if (t > 0) {
        feature.block(0, 0, numSlices, N_) = feature_raw;
        feature_eval.block(t, 0, numSlices, N_) = feature_raw;
    } else if (t == 0) {
        feature = feature_raw;
        feature_eval = feature_raw;
    } else {
        feature.block(-t, 0, numSlices, N_) = feature_raw;
        feature_eval.block(0, 0, numSlices, N_) = feature_raw;
    }*/
    region_.push_back(region);
    region2_.push_back(std::vector<int>());
    compatibility_.push_back(NULL);
    pairwise_.push_back(NULL);
    //addPairwiseEnergy( feature, function, kernel_type, normalization_type, true, feature_eval );
    addPairwiseEnergy(feature_raw, function, kernel_type, normalization_type);
}
void DenseCRF2D::addPairwiseHorizontal(int t, float sp, int numSlices, Matrix<float, Dynamic, Dynamic, RowMajor> & globalBinaryProb, LabelCompatibility * function, std::vector<int> region, KernelType kernel_type, NormalizationType normalization_type) {
    //MatrixXf feature_raw(numSlices, N_);
    MatrixXf feature_raw(1, N_);
    feature_raw.fill(0.0);
    for (int j = 0; j < H_; ++j) {
        for (int i = 0; i < W_; ++i) {
            //feature_raw(numSlices*(W_-1-i)/W_, j*W_+i) = 1 / sp;
            feature_raw(0, j*W_+i) = i / sp;
        }
    }
    
    /*MatrixXf feature = Eigen::MatrixXf::Zero(numSlices + std::abs(t), N_);
    MatrixXf feature_eval = Eigen::MatrixXf::Zero(numSlices + std::abs(t), N_);
    
    if (t > 0) {
        feature.block(0, 0, numSlices, N_) = feature_raw;
        feature_eval.block(t, 0, numSlices, N_) = feature_raw;
    } else if (t == 0) {
        feature = feature_raw;
        feature_eval = feature_raw;
    } else {
        feature.block(-t, 0, numSlices, N_) = feature_raw;
        feature_eval.block(0, 0, numSlices, N_) = feature_raw;
    }*/
    region_.push_back(region);
    region2_.push_back(std::vector<int>());
    compatibility_.push_back(NULL);
    pairwise_.push_back(NULL);
    //addPairwiseEnergy( feature, function, kernel_type, normalization_type, true, feature_eval );
    addPairwiseEnergy(feature_raw, function, kernel_type, normalization_type);
}
/*void DenseCRF2D::addPairwiseInstance(Matrix<float, Dynamic, Dynamic, RowMajor> & globalBinaryProb, LabelCompatibility * function, std::vector<int> region, KernelType kernel_type, NormalizationType normalization_type) {
    region.clear();
    for (int j = 0; j < H_; ++j) {
        for (int i = 0; i < W_; ++i) {
            if (globalBinaryProb(1, j*W_+i) > 0.5) {
                region.push_back(j*W_+i);
            }
        }
    }
    
    MatrixXf feature(1, region.size());
    feature.fill(1.0);

    region_.push_back(region);
    region2_.push_back(std::vector<int>());
    compatibility_.push_back(NULL);
    addPairwiseEnergy(feature, function, kernel_type, normalization_type);
}*/
void DenseCRF2D::addPairwiseInstance(LabelCompatibility * function, std::vector<int> region, std::vector<int> region2, KernelType kernel_type, NormalizationType normalization_type) {
    region_.push_back(region);
    region2_.push_back(region2);
    compatibility_.push_back(function);
    pairwise_.push_back(NULL);
}
void DenseCRF2D::addPairwiseColumn(LabelCompatibility * function, std::vector<int> region, std::vector<int> region2, KernelType kernel_type, NormalizationType normalization_type) {
    region_.push_back(region);
    region2_.push_back(region2);
    compatibility_.push_back(function);
    pairwise_.push_back(NULL);
}
//////////////////////////////
/////  Unary Potentials  /////
//////////////////////////////
void DenseCRF::setUnaryEnergy ( UnaryEnergy * unary ) {
	if( unary_ ) delete unary_;
	unary_ = unary;
}
void DenseCRF::setUnaryEnergy( const MatrixXf & unary ) {
	setUnaryEnergy( new ConstUnaryEnergy( unary ) );
}
void  DenseCRF::setUnaryEnergy( const MatrixXf & L, const MatrixXf & f ) {
	setUnaryEnergy( new LogisticUnaryEnergy( L, f ) );
}
///////////////////////
/////  Inference  /////
///////////////////////
void expAndNormalize ( MatrixXf & out, const MatrixXf & in ) {
	out.resize( in.rows(), in.cols() );
	for( int i=0; i<out.cols(); i++ ){
		VectorXf b = in.col(i);
		b.array() -= b.maxCoeff();
		b = b.array().exp();
		out.col(i) = b / b.array().sum();
	}
}
void sumAndNormalize( MatrixXf & out, const MatrixXf & in, const MatrixXf & Q ) {
	out.resize( in.rows(), in.cols() );
	for( int i=0; i<in.cols(); i++ ){
		VectorXf b = in.col(i);
		VectorXf q = Q.col(i);
		out.col(i) = b.array().sum()*q - b;
	}
}
MatrixXf DenseCRF::inference ( int n_iterations ) const {
    std::cout << "Reaching into DenseCRF::inference, a virtual function" << std::endl;
	MatrixXf Q( M_, N_ ), tmp1, unary( M_, N_ ), tmp2;
	unary.fill(0);
	if( unary_ )
		unary = unary_->get();
	expAndNormalize( Q, -unary );
	
	for( int it=0; it<n_iterations; it++ ) {
		tmp1 = -unary;
		for( unsigned int k=0; k<pairwise_.size(); k++ ) {
			pairwise_[k]->apply( tmp2, Q );
			tmp1 -= tmp2;
		}
		expAndNormalize( Q, tmp1 );
	}
	return Q;
}
VectorXs DenseCRF::map ( int n_iterations ) const {
    std::cout << "Reaching into DenseCRF::map, a virtual function" << std::endl;
	// Run inference
	MatrixXf Q = inference( n_iterations );
	// Find the map
	return currentMap( Q );
}
MatrixXf DenseCRF2D::inference ( int n_iterations, bool step, std::string outFileName ) const {
    MatrixXf Q( M_, N_ ), tmp1, unary( M_, N_ ), tmp2;
    unary.fill(0);
    if( unary_ )
        unary = unary_->get();
    expAndNormalize( Q, -unary );
    
    for( int it=0; it<n_iterations; it++ ) {
        tmp1 = -unary;
        for( unsigned int k = 0; k < region_.size(); ++k ) {
            int region_size = region_[k].size();
            if (region_size) {
                int region_size2 = region2_[k].size();
                if (!region_size2) {
                    // local predictions
                    int y1 = region_[k][0], y2 = region_[k][1], x1 = region_[k][2], x2 = region_[k][3];
                    int H = y2 - y1;
                    int W = x2 - x1;
                    int N = W*H;
                    MatrixXf Qsub( M_, N );
                    for(int j = y1; j < y2; ++j) {
                        Qsub.block(0, (j-y1)*W, M_, W) = Q.block(0, j*W_+x1, M_, W);
                    }
                    
                    pairwise_[k]->apply( tmp2, Qsub );
                    
                    for(int j = y1; j < y2; ++j) {
                        tmp1.block(0, j*W_+x1, M_, W) -= tmp2.block(0, (j-y1)*W, M_, W);
                    }
                } /*else {
                    MatrixXf Qsub(M_, region_size);
                    for(int j = 0; j < region_size; ++j) {
                        Qsub.block(0, j, M_, 1) = Q.block(0, region_[k][j], M_, 1);
                    }
                    
                    pairwise_[k]->apply( tmp2, Qsub );
                    
                    for(int j = 0; j < region_size; ++j) {
                        tmp1.block(0, region_[k][j], M_, 1) -= tmp2.block(0, j, M_, 1);
                    }
                }*/
                else {
                    // inter-instance
                    MatrixXf sumQ(M_, 1);
                    sumQ.fill(0.0f);
                    for(int j = 0; j < region_size2; ++j) {
                        sumQ += Q.block(0, region2_[k][j], M_, 1);
                    }
                    
                    sumQ /= region_size2;
                    compatibility_[k]->apply( sumQ, sumQ );
                    
                    for(int j = 0; j < region_size; ++j) {
                        tmp1.block(0, region_[k][j], M_, 1) -= sumQ;
                    }
                    
                    sumQ.fill(0.0f);
                    for(int j = 0; j < region_size; ++j) {
                        sumQ += Q.block(0, region_[k][j], M_, 1);
                    }
                    
                    sumQ /= region_size;
                    compatibility_[k]->apply( sumQ, sumQ );
                    
                    for(int j = 0; j < region_size2; ++j) {
                        tmp1.block(0, region2_[k][j], M_, 1) -= sumQ;
                    }
                }
            } else {
                if (pairwise_[k]) {
                    // image smoothness
                    pairwise_[k]->apply( tmp2, Q );
                    tmp1 -= tmp2;
                } else {
                    // Column Potential
                    for (int i = 0; i < W_; i++){
                        MatrixXf sumQ(M_, 1);
                        sumQ.fill(0.0f);
                        for (int j = 0; j < H_; ++j) {
                            sumQ += Q.block(0, i+j*W_, M_, 1);
                        }
                        
                        for (int j = 0; j < H_; ++j) {
                            MatrixXf sumQ2 = sumQ - Q.block(0, i+j*W_, M_, 1);
                            sumQ2 /= (H_-1);
                            //sumQ2(0,0) = 0;
                            compatibility_[k]->apply(sumQ2, sumQ2);
                            tmp1.block(0, i+j*W_, M_, 1) -= sumQ2;
                        }
                    }
                }
            }
        }
        expAndNormalize(Q, tmp1);
        
        if (step) {
            VectorXs currMap = currentMap( Q );
            cimg_library::CImg<short> image(currMap.data(), W_, H_, 1, 1, true);
            //image.normalize(0,255);
            std::string ofn = outFileName + "_iter" + std::to_string(it+1) + ".png";
            std::cout << "Saving to: " << ofn << std::endl;
            image.save(ofn.c_str());
        }
    }
    
    /*std::string ofn(outFileName + ".dat");
    std::ofstream out(ofn.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
    out.write((char*)Q.data(), Q.rows()*Q.cols()*sizeof(float));
    out.close();*/
    
    return Q;
}
VectorXs DenseCRF2D::map ( int n_iterations, bool step, std::string outFileName ) const {
    // Run inference
    MatrixXf Q = inference( n_iterations, step, outFileName );
    // Find the map
    return currentMap( Q );
}
///////////////////
/////  Debug  /////
///////////////////
VectorXf DenseCRF::unaryEnergy(const VectorXs & l) {
	assert( l.cols() == N_ );
	VectorXf r( N_ );
	r.fill(0.f);
	if( unary_ ) {
		MatrixXf unary = unary_->get();
		
		for( int i=0; i<N_; i++ )
			if ( 0 <= l[i] && l[i] < M_ )
				r[i] = unary( l[i], i );
	}
	return r;
}
VectorXf DenseCRF::pairwiseEnergy(const VectorXs & l, int term) {
	assert( l.cols() == N_ );
	VectorXf r( N_ );
	r.fill(0.f);
	
	if( term == -1 ) {
		for( unsigned int i=0; i<pairwise_.size(); i++ )
			r += pairwiseEnergy( l, i );
		return r;
	}
	
	MatrixXf Q( M_, N_ );
	// Build the current belief [binary assignment]
	for( int i=0; i<N_; i++ )
		for( int j=0; j<M_; j++ )
			Q(j,i) = (l[i] == j);
	pairwise_[ term ]->apply( Q, Q );
	for( int i=0; i<N_; i++ )
		if ( 0 <= l[i] && l[i] < M_ )
			r[i] =-0.5*Q(l[i],i );
		else
			r[i] = 0;
	return r;
}
MatrixXf DenseCRF::startInference() const{
	MatrixXf Q( M_, N_ );
	Q.fill(0);
	
	// Initialize using the unary energies
	if( unary_ )
		expAndNormalize( Q, -unary_->get() );
	return Q;
}
void DenseCRF::stepInference( MatrixXf & Q, MatrixXf & tmp1, MatrixXf & tmp2 ) const{
	tmp1.resize( Q.rows(), Q.cols() );
	tmp1.fill(0);
	if( unary_ )
		tmp1 -= unary_->get();
	
	// Add up all pairwise potentials
	for( unsigned int k=0; k<pairwise_.size(); k++ ) {
		pairwise_[k]->apply( tmp2, Q );
		tmp1 -= tmp2;
	}
	
	// Exponentiate and normalize
	expAndNormalize( Q, tmp1 );
}
VectorXs DenseCRF::currentMap( const MatrixXf & Q ) const{
	VectorXs r(Q.cols());
	// Find the map
	for( int i=0; i<N_; i++ ){
		int m;
		Q.col(i).maxCoeff( &m );
		r[i] = m;
	}
	return r;
}

// Compute the KL-divergence of a set of marginals
double DenseCRF::klDivergence( const MatrixXf & Q ) const {
	double kl = 0;
	// Add the entropy term
	for( int i=0; i<Q.cols(); i++ )
		for( int l=0; l<Q.rows(); l++ )
			kl += Q(l,i)*log(std::max( Q(l,i), 1e-20f) );
	// Add the unary term
	if( unary_ ) {
		MatrixXf unary = unary_->get();
		for( int i=0; i<Q.cols(); i++ )
			for( int l=0; l<Q.rows(); l++ )
				kl += unary(l,i)*Q(l,i);
	}
	
	// Add all pairwise terms
	MatrixXf tmp;
	for( unsigned int k=0; k<pairwise_.size(); k++ ) {
		pairwise_[k]->apply( tmp, Q );
		kl += (Q.array()*tmp.array()).sum();
	}
	return kl;
}

// Gradient computations
double DenseCRF::gradient( int n_iterations, const ObjectiveFunction & objective, VectorXf * unary_grad, VectorXf * lbl_cmp_grad, VectorXf * kernel_grad) const {
	// Run inference
	std::vector< MatrixXf > Q(n_iterations+1);
	MatrixXf tmp1, unary( M_, N_ ), tmp2;
	unary.fill(0);
	if( unary_ )
		unary = unary_->get();
	expAndNormalize( Q[0], -unary );
	for( int it=0; it<n_iterations; it++ ) {
		tmp1 = -unary;
		for( unsigned int k=0; k<pairwise_.size(); k++ ) {
			pairwise_[k]->apply( tmp2, Q[it] );
			tmp1 -= tmp2;
		}
		expAndNormalize( Q[it+1], tmp1 );
	}
	
	// Compute the objective value
	MatrixXf b( M_, N_ );
	double r = objective.evaluate( b, Q[n_iterations] );
	sumAndNormalize( b, b, Q[n_iterations] );

	// Compute the gradient
	if(unary_grad && unary_)
		*unary_grad = unary_->gradient( b );
	if( lbl_cmp_grad )
		*lbl_cmp_grad = 0*labelCompatibilityParameters();
	if( kernel_grad )
		*kernel_grad = 0*kernelParameters();
	
	for( int it=n_iterations-1; it>=0; it-- ) {
		// Do the inverse message passing
		tmp1.fill(0);
		int ip = 0, ik = 0;
		// Add up all pairwise potentials
		for( unsigned int k=0; k<pairwise_.size(); k++ ) {
			// Compute the pairwise gradient expression
			if( lbl_cmp_grad ) {
				VectorXf pg = pairwise_[k]->gradient( b, Q[it] );
				lbl_cmp_grad->segment( ip, pg.rows() ) += pg;
				ip += pg.rows();
			}
			// Compute the kernel gradient expression
			if( kernel_grad ) {
				VectorXf pg = pairwise_[k]->kernelGradient( b, Q[it] );
				kernel_grad->segment( ik, pg.rows() ) += pg;
				ik += pg.rows();
			}
			// Compute the new b
			pairwise_[k]->applyTranspose( tmp2, b );
			tmp1 += tmp2;
		}
		sumAndNormalize( b, tmp1.array()*Q[it].array(), Q[it] );
		
		// Add the gradient
		if(unary_grad && unary_)
			*unary_grad += unary_->gradient( b );
	}
	return r;
}
VectorXf DenseCRF::unaryParameters() const {
	if( unary_ )
		return unary_->parameters();
	return VectorXf();
}
void DenseCRF::setUnaryParameters( const VectorXf & v ) {
	if( unary_ )
		unary_->setParameters( v );
}
VectorXf DenseCRF::labelCompatibilityParameters() const {
	std::vector< VectorXf > terms;
	for( unsigned int k=0; k<pairwise_.size(); k++ )
		terms.push_back( pairwise_[k]->parameters() );
	int np=0;
	for( unsigned int k=0; k<pairwise_.size(); k++ )
		np += terms[k].rows();
	VectorXf r( np );
	for( unsigned int k=0,i=0; k<pairwise_.size(); k++ ) {
		r.segment( i, terms[k].rows() ) = terms[k];
		i += terms[k].rows();
	}	
	return r;
}
void DenseCRF::setLabelCompatibilityParameters( const VectorXf & v ) {
	std::vector< int > n;
	for( unsigned int k=0; k<pairwise_.size(); k++ )
		n.push_back( pairwise_[k]->parameters().rows() );
	int np=0;
	for( unsigned int k=0; k<pairwise_.size(); k++ )
		np += n[k];
	
	for( unsigned int k=0,i=0; k<pairwise_.size(); k++ ) {
		pairwise_[k]->setParameters( v.segment( i, n[k] ) );
		i += n[k];
	}	
}
VectorXf DenseCRF::kernelParameters() const {
	std::vector< VectorXf > terms;
	for( unsigned int k=0; k<pairwise_.size(); k++ )
		terms.push_back( pairwise_[k]->kernelParameters() );
	int np=0;
	for( unsigned int k=0; k<pairwise_.size(); k++ )
		np += terms[k].rows();
	VectorXf r( np );
	for( unsigned int k=0,i=0; k<pairwise_.size(); k++ ) {
		r.segment( i, terms[k].rows() ) = terms[k];
		i += terms[k].rows();
	}	
	return r;
}
void DenseCRF::setKernelParameters( const VectorXf & v ) {
	std::vector< int > n;
	for( unsigned int k=0; k<pairwise_.size(); k++ )
		n.push_back( pairwise_[k]->kernelParameters().rows() );
	int np=0;
	for( unsigned int k=0; k<pairwise_.size(); k++ )
		np += n[k];
	
	for( unsigned int k=0,i=0; k<pairwise_.size(); k++ ) {
		pairwise_[k]->setKernelParameters( v.segment( i, n[k] ) );
		i += n[k];
	}	
}
